<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html >
<head>
<meta http-equiv="Cache-control" content="public">
<link rel="stylesheet" type="text/css" href="/StockPlusEngine/style/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/StockPlusEngine/style/font-awesome.min.css" />


<script src="/StockPlusEngine/script/jquery-1.11.1.min.js" 		type="text/javascript"><!-- required for FF3 and Opera --></script>
<script src="/StockPlusEngine/script/bootstrap.min.js" 			type="text/javascript"><!-- required for FF3 and Opera --></script>
    
    
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NO ITEM FOUND !</title>
<style type="text/css">
	h1 {
	    font-size: 80px;
	    line-height: 1;
	}
	h2{
	    font-size: 50px;
	    line-height: 1;
	}
	h3{
	    font-size: 120px;
	    line-height: 1;
	}
	h4{
	    font-size: 30px;
	    line-height: 1;
	}
	h5{
	    font-size: 200px;
	    line-height: 1;
	}
	
	.btn-lg-text{
	    font-size: 63px;
	    line-height: 1;
	    margin-left: 20px;
	}
</style>
</head>
<body>
<div id="cancel-div" class="container">
	<div class="row" style="color:red;text-align: center;margin-top: 50px">
		<h5><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></h5>
	</div>
	<div class="row" style="text-align: center;">
		<h1><label style="color:black;" id="item_code" >NO ITEM FOUND !</label></h1>
	</div>
</div>



</body>
</html>