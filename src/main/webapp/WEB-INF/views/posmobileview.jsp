<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html >
<head>
<link rel="stylesheet" type="text/css" href="/StockPlusEngine/style/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/StockPlusEngine/style/font-awesome.min.css" />


<script src="/StockPlusEngine/script/jquery-1.11.1.min.js" 		type="text/javascript"><!-- required for FF3 and Opera --></script>
<script src="/StockPlusEngine/script/bootstrap.min.js" 			type="text/javascript"><!-- required for FF3 and Opera --></script>
    
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>POS</title>
<style type="text/css">
	h1 {
	    font-size: 80px;
	    line-height: 1;
	}
	h2{
	    font-size: 50px;
	    line-height: 1;
	}
	h3{
	    font-size: 120px;
	    line-height: 1;
	}
	h4{
	    font-size: 30px;
	    line-height: 1;
	}
	h5{
	    font-size: 200px;
	    line-height: 1;
	}
	
	.btn-lg-text{
	    font-size: 63px;
	    line-height: 1;
	    margin-left: 20px;
	}
</style>
</head>
<body>
<div id="one" class="container" >
	<div class="row">
	   <input type="hidden" id="item_id" value="${item.id }" />
	   <input type="hidden" id="saler" value="${saler }" />
		<h1><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span> <label style="color:blue;" id="item_barcode" >${item.itemBarcode }</label></h1>
	</div>
	<div class="row">
		<h1><label style="color:black;">Item : </label> <label style="color:blue;" id="item_code" >${item.itemCode }</label></h1>
	</div>
	<div class="row">
		<h2><label  style="color:blue;" id="item_name" >${item.name }</label></h2>
	</div>
	<div class="row" style="margin-top: 30px;" >
		<div class="input-group" style="color:black;text-align: right;">
		  <h3><input type="number" class="form-control" name="quantity" id="quantity" style="height: 234px;width:700px;font-size: inherit;text-align: right;border-color: #35d2d2;" value="1" ></h3>
		  <h4><label  style="color:black;text-align: right;">Piece.</label></h4>
		</div>
	</div>
	<div class="row" style="margin-top: 30px;" >
		<button type="button" class="btn btn-success btn-lg btn-lg-text" id="button_confirm" >
    		Confirm
    		<span class="fa fa-check" aria-hidden="true"></span>
  		</button>
		<button type="button" class="btn btn-danger btn-lg btn-lg-text" id="button_cancel" >
    		Cancel
    		<span class="glyphicon glyphicon-remove" aria-hidden="true" ></span>
  		</button>
	</div>
	
	
</div>
<div id="success-div" class="container hide">
	<div class="row" style="color:green;text-align: center;margin-top: 50px">
		<h5><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></h5>
	</div>
	<div class="row" style="text-align: center;">
		<h1><label style="color:black;"  >SEND SUCCESS</label></h1>
	</div>
</div>
<div id="cancel-div" class="container hide">
	<div class="row" style="color:red;text-align: center;margin-top: 50px">
		<h5><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></h5>
	</div>
	<div class="row" style="text-align: center;">
		<h1><label style="color:black;"  >CANCEL</label></h1>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function () {
	
	$('#button_confirm').on("click",function(e){
		var jsonParams = {};
		jsonParams['itemId']       = $('#item_id').val();
		jsonParams['quantity']     = $('#quantity').val();
		jsonParams['saler']        = $('#saler').val();
		
		var itemData = $.ajax({
	        type: "POST",
	        headers: {
	            Accept: 'application/json'
	        },
	        url: 'additem',
	        data: jsonParams,
	        complete: function (xhr) {
	        	$('#one').addClass('hide');
				$('#success-div').removeClass('hide');
	        }
	    }).done(function (){
	    	//
	    });
    });
	

	$('#button_cancel').on("click",function(e){
		$('#one').addClass('hide');
		$('#cancel-div').removeClass('hide');
    });
});
</script>


</body>
</html>