﻿
 
SELECT  b.saleperiod,
        monthyear.periodmonth as salemonthnum,
        monthyear.periodmonthdisp as salemonthnumdisp,
        monthyear.monthname as monthname,
				b.salemonth,
				monthyear.datayear,
				b.subtotalamount,
				b.rebateamount,
				b.vatamount,
				b.totalamount,
				b.subtotalafterrebate,
				b.itemcost,
				b.contamount,
				b.marginamount
FROM(	  SELECT years.yy as datayear,concat(years.yy,months.num) as periodmonth,months.mname as monthname,concat(years.yy,months.numdisp) as periodmonthdisp
				FROM (SELECT DISTINCT date_format(so.`so_sale_time`,'%Y') as yy FROM tns_sale_order so where so.`so_sale_time` IS NOT NULL) years
				CROSS JOIN(	select '401'  as num,'403'  as numdisp,'Jan' as mname  union all 
						select '402' as num,'402'  as numdisp,'Feb' as mname   union all 
						select '403'  as num,'401'  as numdisp,'Mar' as mname  union all 
						select '304' as num,'303'  as numdisp,'Apr' as mname   union all 
						select '305'  as num,'302'  as numdisp,'May' as mname  union all 
						select '306' as num,'301'  as numdisp,'Jun' as mname   union all 
						select '207'  as num,'203'  as numdisp,'Jul' as mname  union all 
						select '208' as num,'202'  as numdisp,'Aug' as mname   union all 
						select '209'  as num,'201'  as numdisp,'Sep' as mname  union all 
						select '110' as num,'103'  as numdisp,'Oct' as mname   union all 
						select '111'  as num,'102'  as numdisp,'Nov' as mname  union all 
						select '112' as num,'101'  as numdisp,'Dec' as mname ) months ) monthyear
LEFT OUTER JOIN (SELECT CONCAT(
																	date_format(a.`so_sale_time`,'%Y'),
																	5-CEIL((date_format(a.`so_sale_time`,'%m')/3)),
																	date_format(a.`so_sale_time`,'%m')
																) as saleperiod,
												 date_format(a.`so_sale_time`,'%c') as salemonthnum,
												 date_format(a.`so_sale_time`,'%Y%m') as salemonth,
												 date_format(a.`so_sale_time`,'%Y') as saleyear,
												 SUM(a.so_subtotal_amt) as subtotalamount,
												 SUM(a.so_rebate_amt) as rebateamount,
												 SUM(a.so_vat_amt) as vatamount,
												 SUM(a.so_total_amt) as totalamount,
												 SUM(a.so_subtotal_amt_after_rebate) as subtotalafterrebate,
												 SUM(a.so_item_cost_amt) as itemcost,
												 SUM(a.so_cont_amt) as contamount,
												 SUM(a.so_margin_amt) as marginamount
								FROM tns_sale_order a
								GROUP BY date_format(a.`so_sale_time`,'%Y%m')) b
on b.saleperiod = monthyear.periodmonth
UNION 
SELECT  b.saleperiod,
        monthyear.periodmonth as salemonthnum,
        monthyear.periodmonthdisp as salemonthnumdisp,
        monthyear.monthname as monthname,
				b.salemonth,
				monthyear.datayear,
				b.subtotalamount,
				b.rebateamount,
				b.vatamount,
				b.totalamount,
				b.subtotalafterrebate,
				b.itemcost,
				b.contamount,
				b.marginamount
FROM(	  SELECT years.yy as datayear,concat(years.yy,months.num) as periodmonth,months.mname as monthname,concat(years.yy,months.numdisp) as periodmonthdisp
				FROM (SELECT DISTINCT date_format(so.`so_sale_time`,'%Y') as yy FROM tns_sale_order so where so.`so_sale_time` IS NOT NULL) years
				CROSS JOIN(	select '400'  as num,'400'  as numdisp,'Quarter 1' as mname  union all 
										select '300' as num,'300'  as numdisp,'Quarter 2' as mname   union all 
										select '200'  as num,'200'  as numdisp,'Quarter 3' as mname  union all 
										select '100' as num,'100'  as numdisp,'Quarter 4' as mname ) months ) monthyear
LEFT OUTER JOIN (SELECT CONCAT(
																		date_format(a.`so_sale_time`,'%Y'),
																		5-CEIL((date_format(a.`so_sale_time`,'%m')/3)),
                                    '00'
																	) as saleperiod,
												 date_format(a.`so_sale_time`,'%c') as salemonthnum,
												 date_format(a.`so_sale_time`,'%Y%m') as salemonth,
												 date_format(a.`so_sale_time`,'%Y') as saleyear,
												 SUM(a.so_subtotal_amt) as subtotalamount,
												 SUM(a.so_rebate_amt) as rebateamount,
												 SUM(a.so_vat_amt) as vatamount,
												 SUM(a.so_total_amt) as totalamount,
												 SUM(a.so_subtotal_amt_after_rebate) as subtotalafterrebate,
												 SUM(a.so_item_cost_amt) as itemcost,
												 SUM(a.so_cont_amt) as contamount,
												 SUM(a.so_margin_amt) as marginamount
								FROM tns_sale_order a
								GROUP BY CONCAT(date_format(a.`so_sale_time`,'%Y'),CEIL((date_format(a.`so_sale_time`,'%m')/3)))) b
on b.saleperiod = monthyear.periodmonth
UNION 
SELECT date_format(a.`so_sale_time`,'%Y') as saleperiod,
       null as salemonthnum,
       date_format(a.`so_sale_time`,'%Y') as salemonthnumdisp,
       CONCAT('Summary ',date_format(a.`so_sale_time`,'%Y')) as monthname,
       date_format(a.`so_sale_time`,'%Y%m') as salemonth,
       date_format(a.`so_sale_time`,'%Y') as saleyear,
       SUM(a.so_subtotal_amt) as subtotalamount,
       SUM(a.so_rebate_amt) as rebateamount,
       SUM(a.so_vat_amt) as vatamount,
       SUM(a.so_total_amt) as totalamount,
       SUM(a.so_subtotal_amt_after_rebate) as subtotalafterrebate,
			 SUM(a.so_item_cost_amt) as itemcost,
       SUM(a.so_cont_amt) as contamount,
			 SUM(a.so_margin_amt) as marginamount
FROM tns_sale_order a
GROUP BY date_format(a.`so_sale_time`,'%Y')