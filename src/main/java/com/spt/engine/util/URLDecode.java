package com.spt.engine.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class URLDecode {

	public static String decode(String str) {
		String result = null;
		if(str!=null){
			try {
				result= URLDecoder.decode(str, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
}
