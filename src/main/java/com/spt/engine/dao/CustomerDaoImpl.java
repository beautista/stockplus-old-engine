package com.spt.engine.dao;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.spt.engine.entity.Customer;
import com.spt.engine.entity.Item;
import com.spt.engine.entity.RunningNumber;

@Repository
@Transactional
public class CustomerDaoImpl   extends HibernateDaoSupport implements CustomerDao {
	static final Logger LOGGER = LoggerFactory.getLogger(CustomerDaoImpl.class);
	
	@Autowired
	public CustomerDaoImpl(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public Session getSession() {
	        return getSessionFactory().getCurrentSession();
	}

	@Override
	public Customer getCustomerById(Long id) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().get(Customer.class, id);
	}

	@Override
	public List<Customer> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(Customer.class, "customer");
		
		if(!String.valueOf(criteriaMap.get("firstname")).equals("null")){
			criteria.add(Restrictions.ilike("firstname", "%"+String.valueOf(criteriaMap.get("firstname")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("customerBarcode")).equals("null")){
			criteria.add(Restrictions.ilike("customerBarcode", "%"+String.valueOf(criteriaMap.get("customerBarcode")+"%")));
		}
		criteria.addOrder(Order.desc("customerCode"));
		List<Customer> result = (List<Customer>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public Integer findCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Criteria criteria = getSession().createCriteria(Customer.class, "customer");
				
				if(!String.valueOf(criteriaMap.get("firstname")).equals("null")){
					criteria.add(Restrictions.ilike("firstname", "%"+String.valueOf(criteriaMap.get("firstname")+"%")));
				}
				if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
					criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
				}
				if(!String.valueOf(criteriaMap.get("customerBarcode")).equals("null")){
					criteria.add(Restrictions.ilike("customerBarcode", "%"+String.valueOf(criteriaMap.get("customerBarcode")+"%")));
				}
				
				criteria.setProjection(Projections.count("id"));
				Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
				return result;
	}

	@Override
	public Customer getCustomerByCode(String code) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
        DetachedCriteria criteria = DetachedCriteria.forClass(Customer.class, "customer");
		
        criteria.add(Restrictions.ilike("customerCode", code));
        List<Customer> requltLs =  (List<Customer>) getHibernateTemplate().findByCriteria(criteria);
        Customer itemReturn = null;
        if(requltLs!=null && requltLs.size()>0){
        	itemReturn = requltLs.get(0);
        }
		
		return itemReturn;
	}

	@Override
	public void save(Customer customer) {
		// TODO Auto-generated method stub
		String customerCode = null;
		if("null".equals(String.valueOf(customer.getCustomerCode())) ||
				"".equals(String.valueOf(customer.getCustomerCode())) ||
				"AUTO".equals(customer.getCustomerCode())){
			customerCode = getMaxCustomerCode();
			customer.setCustomerCode(customerCode);
		}
		
		if("null".equals(String.valueOf(customer.getCustomerBarcode())) ||
				"".equals(String.valueOf(customer.getCustomerBarcode())) ||
				"AUTO".equals(customer.getCustomerBarcode())){
			customer.setCustomerBarcode(customerCode);
		}
		
		if("null".equals(String.valueOf(customer.getPoint())) ||
				"".equals(String.valueOf(customer.getPoint())) ){
			customer.setPoint(0);
		}

		getHibernateTemplate().saveOrUpdate(customer);
	}
	
	private synchronized String getMaxCustomerCode() {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(RunningNumber.class, "running");
		criteria.add(Restrictions.ilike("code", "CUSTOMER_CODE"));
		List<RunningNumber> requltLs =  (List<RunningNumber>) getHibernateTemplate().findByCriteria(criteria);
		RunningNumber runningNumber = null;
        if(requltLs!=null && requltLs.size()>0){
        	runningNumber = requltLs.get(0);
        }
        
        if(runningNumber!=null){
        	runningNumber.setRunning(runningNumber.getRunning()+1);
        }else{
        	runningNumber = new RunningNumber();
        	runningNumber.setCode("CUSTOMER_CODE");
        	runningNumber.setRunning(1);
        }

    	getHibernateTemplate().saveOrUpdate(runningNumber);
		
		return String.format("%010d", runningNumber.getRunning());
	}

	@Override
	public void deleteCustomerById(Long id) {
		// TODO Auto-generated method stub

		getHibernateTemplate().delete(getHibernateTemplate().get(Customer.class, id));
	}

}
