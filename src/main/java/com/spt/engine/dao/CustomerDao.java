package com.spt.engine.dao;

import java.util.List;
import java.util.Map;

import com.spt.engine.entity.Customer;

public interface CustomerDao {

	public Customer getCustomerById(Long id);
	public List<Customer> findByCriteria(Map<String,Object> criteriaMap);
	public Integer findCustomerSize(Map<String,Object> criteriaMap);
	public Customer getCustomerByCode(String code);
	public void save(Customer customer);
	public void deleteCustomerById(Long id);
	
}
