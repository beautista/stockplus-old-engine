package com.spt.engine.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.spt.engine.entity.Customer;
import com.spt.engine.entity.RunningNumber;
import com.spt.engine.entity.SaleOrder;
import com.spt.engine.entity.SaleOrderItems;

@Repository
@Transactional
public class SalesOrderDaoImpl extends HibernateDaoSupport implements SalesOrderDao {

	static final Logger LOGGER = LoggerFactory.getLogger(SalesOrderDaoImpl.class);
	
	@Autowired
	public SalesOrderDaoImpl(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public Session getSession() {
	        return getSessionFactory().getCurrentSession();
	}
	
	@Override
	public SaleOrder getSaleOrderById(Long id) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().get(SaleOrder.class, id);
	}

	@Override
	public List<SaleOrder> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrder.class, "saleOrder");
		
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		

		if(!String.valueOf(criteriaMap.get("saleDateString")).equals("null")){
			criteria.add(Restrictions.ilike("saleDateString", "%"+String.valueOf(criteriaMap.get("saleDateString")+"%")));
		}
		

		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		/*when set projection */
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("id"),"id")
	            .add(Projections.property("soNumber"),"soNumber")
	            .add(Projections.property("saler"),"saler")   
	            .add(Projections.property("saleDate"),"saleDate") 
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("itemCount"),"itemCount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            .add(Projections.property("customerCode"),"customerCode")
	            
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("subTotalAfterRebateAmount"),"subTotalAfterRebateAmount")
	            .add(Projections.property("subTotalAfterVatAmount"),"subTotalAfterVatAmount")
	            .add(Projections.property("tendedAmount"),"tendedAmount")
	            .add(Projections.property("changeAmount"),"changeAmount")
	            .add(Projections.property("roundType"),"roundType")
	            .add(Projections.property("paymentType"),"paymentType")
	            
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("fixCostPercentage"),"fixCostPercentage")
	            .add(Projections.property("fixCostAmount"),"fixCostAmount")
	            .add(Projections.property("contributionPercentage"),"contributionPercentage")
	            .add(Projections.property("contributionAmount"),"contributionAmount")
	            .add(Projections.property("marginPercentage"),"marginPercentage")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            
	            
	            );   
		criteria.setResultTransformer( Transformers.aliasToBean(SaleOrder.class));
		
		
		criteria.addOrder(Order.desc("soNumber"));
		List<SaleOrder> result = (List<SaleOrder>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public Integer findSaleOrderSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		Criteria criteria = getSession().createCriteria(SaleOrder.class, "saleOrder");
		

		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("saleDateString")).equals("null")){
			criteria.add(Restrictions.ilike("saleDateString", "%"+String.valueOf(criteriaMap.get("saleDateString")+"%")));
		}


		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		return result;
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(criteriaMap.get("soId")).equals("null")){
			criteria.add(Restrictions.eq("soId", criteriaMap.get("soId")));
		}
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		
		List<SaleOrderItems> result = (List<SaleOrderItems>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public Integer findSaleOrderItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		Criteria criteria = getSession().createCriteria(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(criteriaMap.get("soId")).equals("null")){
			criteria.add(Restrictions.eq("soId", criteriaMap.get("soId")));
		}
		
		if(!String.valueOf(criteriaMap.get("soNumber")).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+String.valueOf(criteriaMap.get("soNumber")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		return result;
	}
	
	@Override
	public List<SaleOrderItems> findSaleOrderItemCustomerByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		criteria.addOrder(Order.desc("createdDate"));
		criteria.addOrder(Order.desc("itemCode"));
		
		
		List<SaleOrderItems> result = (List<SaleOrderItems>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public Integer findSaleOrderItemCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		Criteria criteria = getSession().createCriteria(SaleOrderItems.class, "saleOrderItems");
		
		
		if(!String.valueOf(criteriaMap.get("customerCode")).equals("null")){
			criteria.add(Restrictions.ilike("customerCode", "%"+String.valueOf(criteriaMap.get("customerCode")+"%")));
		}
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		return result;
	}

	@Override
	public SaleOrder getSaleOrderByNumber(String number) {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrder.class, "saleOrder");
		
        criteria.add(Restrictions.ilike("soNumber", number));
        
        /*when set projection */
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("id"),"id")
	            .add(Projections.property("soNumber"),"soNumber")
	            .add(Projections.property("saler"),"saler")   
	            .add(Projections.property("saleDate"),"saleDate") 
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("itemCount"),"itemCount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("subTotalAfterRebateAmount"),"subTotalAfterRebateAmount")
	            .add(Projections.property("subTotalAfterVatAmount"),"subTotalAfterVatAmount")
	            .add(Projections.property("tendedAmount"),"tendedAmount")
	            .add(Projections.property("changeAmount"),"changeAmount")
	            .add(Projections.property("roundType"),"roundType")
	            .add(Projections.property("paymentType"),"paymentType")
	            
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("fixCostPercentage"),"fixCostPercentage")
	            .add(Projections.property("fixCostAmount"),"fixCostAmount")
	            .add(Projections.property("contributionPercentage"),"contributionPercentage")
	            .add(Projections.property("contributionAmount"),"contributionAmount")
	            .add(Projections.property("marginPercentage"),"marginPercentage")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            
	            
	            );   
		criteria.setResultTransformer( Transformers.aliasToBean(SaleOrder.class));
        
        List<SaleOrder> requltLs =  (List<SaleOrder>) getHibernateTemplate().findByCriteria(criteria);
        SaleOrder itemReturn = null;
        if(requltLs!=null && requltLs.size()>0){
        	itemReturn = requltLs.get(0);
        }
		
		return itemReturn;
	}

	@Override
	public void save(SaleOrder saleOrder) {
		// TODO Auto-generated method stub
		//Save Header
		String soNumber = "";
		if("null".equals(String.valueOf(saleOrder.getSoNumber())) ||
				"".equals(String.valueOf(saleOrder.getSoNumber())) ||
				"AUTO".equals(saleOrder.getSoNumber())){
			soNumber = getMaxSoNumber();
			saleOrder.setSoNumber(soNumber);
		}
		
		//Save Detail
		getHibernateTemplate().save(saleOrder);
	}
	
	private synchronized String getMaxSoNumber() {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(RunningNumber.class, "running");
		criteria.add(Restrictions.ilike("code", "SO_NUMBER"));
		List<RunningNumber> requltLs =  (List<RunningNumber>) getHibernateTemplate().findByCriteria(criteria);
		RunningNumber runningNumber = null;
        if(requltLs!=null && requltLs.size()>0){
        	runningNumber = requltLs.get(0);
        }
        
        if(runningNumber!=null){
        	runningNumber.setRunning(runningNumber.getRunning()+1);
        }else{
        	runningNumber = new RunningNumber();
        	runningNumber.setCode("SO_NUMBER");
        	runningNumber.setRunning(1);
        }

    	getHibernateTemplate().saveOrUpdate(runningNumber);
		
		return String.format("%06d", runningNumber.getRunning());
	}

	@Override
	public void deleteSaleOrderById(Long id) {
		// TODO Auto-generated method stub
		//Clear Header
		getHibernateTemplate().deleteAll(findSaleOrderItemBySoId(id));

		//Clear Detail
		getHibernateTemplate().delete(getHibernateTemplate().get(SaleOrder.class, id));
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoNumber(String soNumber) {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrder");
		
        criteria.add(Restrictions.ilike("soNumber", soNumber));
       
        return  (List<SaleOrderItems>) getHibernateTemplate().findByCriteria(criteria);
	}
	
	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoId(Long soId) {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrder");
		
        criteria.add(Restrictions.ilike("soId", soId));
       
        return  (List<SaleOrderItems>) getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public void save(SaleOrderItems saleOrderItems) {
		// TODO Auto-generated method stub
		getHibernateTemplate().save(saleOrderItems);
	}

	@Override
	public void deleteSaleOrderItemsById(Long id) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(getHibernateTemplate().get(SaleOrderItems.class, id));
	}

	@Override
	public List<SaleOrderItems> getSaleOrderItemByNumber(String number) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(SaleOrderItems.class, "saleOrderItems");
		
		if(!String.valueOf(number).equals("null")){
			criteria.add(Restrictions.ilike("soNumber", "%"+number+"%"));
		}
		
		
		List<SaleOrderItems> result = (List<SaleOrderItems>) getHibernateTemplate().findByCriteria(criteria);
		
		LOGGER.debug("result="+result);
		return result;
	}

}
