package com.spt.engine.dao;


import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.spt.engine.controller.ItemController;
import com.spt.engine.entity.Item;
import com.spt.engine.entity.RunningNumber;

@Repository
@Transactional
public class ItemDaoImpl  extends HibernateDaoSupport implements ItemDao{ 

	static final Logger LOGGER = LoggerFactory.getLogger(ItemDaoImpl.class);
	
	@Autowired
	public ItemDaoImpl(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public Session getSession() {
	        return getSessionFactory().getCurrentSession();
	}

	@Override
	public Item getItemById(Long id) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().get(Item.class, id);
	}

	@Override
	public List<Item> findByCriteria(Map<String,Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(Item.class, "item");
		
		if(!String.valueOf(criteriaMap.get("name")).equals("null")){
			criteria.add(Restrictions.ilike("name", "%"+String.valueOf(criteriaMap.get("name")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("barcode")).equals("null")){
			criteria.add(Restrictions.ilike("itemBarcode", "%"+String.valueOf(criteriaMap.get("barcode")+"%")));
		}
		criteria.addOrder(Order.desc("itemCode"));
		List<Item> result = (List<Item>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		
		return result;
	}

	@Override
	public Integer findItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		Criteria criteria = getSession().createCriteria(Item.class, "item");
		
		if(!String.valueOf(criteriaMap.get("name")).equals("null")){
			criteria.add(Restrictions.ilike("name", "%"+String.valueOf(criteriaMap.get("name")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		if(!String.valueOf(criteriaMap.get("barcode")).equals("null")){
			criteria.add(Restrictions.ilike("itemBarcode", "%"+String.valueOf(criteriaMap.get("barcode")+"%")));
		}
		
		criteria.setProjection(Projections.count("id"));
		Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
		return result;
	}

	@Override
	public Item getItemByBarcode(String barcode) {
		// TODO Auto-generated method stub
        DetachedCriteria criteria = DetachedCriteria.forClass(Item.class, "item");
		
        criteria.add(Restrictions.ilike("itemBarcode", barcode));
        List<Item> requltLs =  (List<Item>) getHibernateTemplate().findByCriteria(criteria);
        Item itemReturn = null;
        if(requltLs!=null && requltLs.size()>0){
        	itemReturn = requltLs.get(0);
        }
		
		return itemReturn;
	}

	@Override
	public void save(Item item) {
		// TODO Auto-generated method stub
		String itemCode = null;
		if("null".equals(String.valueOf(item.getItemCode())) ||
				"".equals(String.valueOf(item.getItemCode())) ||
				"AUTO".equals(item.getItemCode())){
			itemCode = getMaxItemCode();
			item.setItemCode(itemCode);
		}
		

		if("null".equals(String.valueOf(item.getItemBarcode())) ||
				"".equals(String.valueOf(item.getItemBarcode())) ||
				"AUTO".equals(item.getItemBarcode())){
			item.setItemBarcode(String.format("%013d", Integer.parseInt(itemCode)));
		}
		
		if("null".equals(String.valueOf(item.getStockOnHand())) ||
				"".equals(String.valueOf(item.getStockOnHand())) ){
			item.setStockOnHand(0);
		}
		
		if("null".equals(String.valueOf(item.getMinimumStock())) ||
				"".equals(String.valueOf(item.getMinimumStock())) ){
			item.setMinimumStock(0);
		}
		
		if("null".equals(String.valueOf(item.getSellingPriceAmount())) ||
				"".equals(String.valueOf(item.getSellingPriceAmount())) ){
			item.setSellingPriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPurchasePriceAmount())) ||
				"".equals(String.valueOf(item.getPurchasePriceAmount())) ){
			item.setPurchasePriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getNormalSellingPriceAmount())) ||
				"".equals(String.valueOf(item.getNormalSellingPriceAmount())) ){
			item.setNormalSellingPriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionSellingPriceAmount())) ||
				"".equals(String.valueOf(item.getPromotionSellingPriceAmount())) ){
			item.setPromotionSellingPriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionSellingPricePercentage())) ||
				"".equals(String.valueOf(item.getPromotionSellingPricePercentage())) ){
			item.setPromotionSellingPricePercentage(0.0f);
		}
		if("null".equals(String.valueOf(item.getNormalPurchasePriceAmount())) ||
				"".equals(String.valueOf(item.getNormalPurchasePriceAmount())) ){
			item.setNormalPurchasePriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionPurchasePriceAmount())) ||
				"".equals(String.valueOf(item.getPromotionPurchasePriceAmount())) ){
			item.setPromotionPurchasePriceAmount(0.0f);
		}
		if("null".equals(String.valueOf(item.getPromotionPurchasePricePercentage())) ||
				"".equals(String.valueOf(item.getPromotionPurchasePricePercentage())) ){
			item.setPromotionPurchasePricePercentage(0.0f);
		}
		
		getHibernateTemplate().saveOrUpdate(item);
		
		
	}
	
	
	private synchronized String getMaxItemCode() {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(RunningNumber.class, "running");
		criteria.add(Restrictions.ilike("code", "ITEM_CODE"));
		List<RunningNumber> requltLs =  (List<RunningNumber>) getHibernateTemplate().findByCriteria(criteria);
		RunningNumber runningNumber = null;
        if(requltLs!=null && requltLs.size()>0){
        	runningNumber = requltLs.get(0);
        }
        
        if(runningNumber!=null){
        	runningNumber.setRunning(runningNumber.getRunning()+1);
        }else{
        	runningNumber = new RunningNumber();
        	runningNumber.setCode("ITEM_CODE");
        	runningNumber.setRunning(1);
        }

    	getHibernateTemplate().saveOrUpdate(runningNumber);
		
		return String.format("%08d", runningNumber.getRunning());
	}

	@Override
	public void deleteItemById(Long id) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(getHibernateTemplate().get(Item.class, id));
	}


	
}
