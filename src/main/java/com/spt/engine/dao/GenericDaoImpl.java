package com.spt.engine.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.spt.engine.entity.StockHistory;

@Repository
@Transactional
public class GenericDaoImpl   extends HibernateDaoSupport implements GenericDao {

static final Logger LOGGER = LoggerFactory.getLogger(GenericDaoImpl.class);
	
	@Autowired
	public GenericDaoImpl(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public Session getSession() {
	        return getSessionFactory().getCurrentSession();
	}

	@Override
	public void save(Object entity) {
		// TODO Auto-generated method stub
		getHibernateTemplate().saveOrUpdate(entity);
	}

	@Override
	public void deleteById(String entityName,Long id) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(getHibernateTemplate().get(entityName, id));
	}

	@Override
	public void delete(Object entity) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(entity);
	}


}
