package com.spt.engine.dao;

import java.util.List;
import java.util.Map;

import com.spt.engine.entity.SaleOrder;
import com.spt.engine.entity.SaleOrderItems;

public interface SalesOrderDao {

	public SaleOrder getSaleOrderById(Long id);
	
	public List<SaleOrder> findByCriteria(Map<String,Object> criteriaMap);
	public Integer findSaleOrderSize(Map<String,Object> criteriaMap);
	public List<SaleOrderItems> findSaleOrderItemByCriteria(Map<String,Object> criteriaMap);
	public Integer findSaleOrderItemSize(Map<String,Object> criteriaMap);
	public List<SaleOrderItems> findSaleOrderItemCustomerByCriteria(Map<String,Object> criteriaMap);
	public Integer findSaleOrderItemCustomerSize(Map<String,Object> criteriaMap);
	
	public SaleOrder getSaleOrderByNumber(String number);
	public List<SaleOrderItems> getSaleOrderItemByNumber(String number);
	public void save(SaleOrder saleOrder);
	public void save(SaleOrderItems saleOrderItems);
	public void deleteSaleOrderById(Long id);
	public void deleteSaleOrderItemsById(Long id);
	public List<SaleOrderItems> findSaleOrderItemBySoNumber(String soNumber);
	public List<SaleOrderItems> findSaleOrderItemBySoId(Long soId);
	
}
