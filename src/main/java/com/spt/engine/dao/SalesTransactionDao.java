package com.spt.engine.dao;

import java.util.List;
import java.util.Map;

import com.spt.engine.entity.Item;
import com.spt.engine.entity.SalesTransaction;

public interface SalesTransactionDao {

	public SalesTransaction getById(Long id);
	public List<SalesTransaction> findByCriteria(Map<String,Object> criteriaMap);
	public Integer findSize(Map<String, Object> criteriaMap);
	public void save(SalesTransaction salesTransaction);
	public void deleteSalesTransactionById(Long id);
	public void deleteSalesTransactionByUser(String saler);
	public List<SalesTransaction> findBySaler(String saler);
	
}
