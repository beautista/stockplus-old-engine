package com.spt.engine.dao;

import java.util.List;
import java.util.Map;

import com.spt.engine.entity.Item;
import com.spt.engine.entity.SalesTransaction;
import com.spt.engine.entity.StockHistory;

public interface GenericDao {

	public void save(Object entity);
	public void deleteById(String entityName,Long id);
	public void delete(Object entity);
	
}
