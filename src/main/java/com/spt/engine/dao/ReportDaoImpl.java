package com.spt.engine.dao;

import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import com.spt.engine.entity.ReportItem;
import com.spt.engine.entity.ReportSale;
import com.spt.engine.entity.ReportSummary;

@Repository
@Transactional
public class ReportDaoImpl extends HibernateDaoSupport implements ReportDao {
	static final Logger LOGGER = LoggerFactory.getLogger(ReportDaoImpl.class);
	
	@Autowired
	public ReportDaoImpl(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public Session getSession() {
	        return getSessionFactory().getCurrentSession();
	}

	@Override
	public List findByCriteriaReportItem(Map<String, Object> criteriaMap) {
		int firstResult = 0;
		int maxResults = 10;
		
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportItem.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemBarcode")).equals("null")){
			criteria.add(Restrictions.ilike("itemBarcode", "%"+String.valueOf(criteriaMap.get("itemBarcode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemName")).equals("null")){
			criteria.add(Restrictions.ilike("itemName", "%"+String.valueOf(criteriaMap.get("itemName")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("itemCode"),"itemCode")
	            .add(Projections.groupProperty("itemBarcode"),"itemBarcode")
	            .add(Projections.groupProperty("itemName"),"itemName")
	            .add(Projections.sum("quantity"),"quantity")
	            .add(Projections.groupProperty("stockOnhand"),"stockOnhand")
	            .add(Projections.groupProperty("minimumStock"),"minimumStock")
		
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportItem.class));
		criteria.addOrder(Order.desc("quantity"));
		List<ReportItem> result = (List<ReportItem>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public Integer findItemSizeReportItem(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportItem.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemBarcode")).equals("null")){
			criteria.add(Restrictions.ilike("itemBarcode", "%"+String.valueOf(criteriaMap.get("itemBarcode")+"%")));
		}
		
		if(!String.valueOf(criteriaMap.get("itemName")).equals("null")){
			criteria.add(Restrictions.ilike("itemName", "%"+String.valueOf(criteriaMap.get("itemName")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("itemCode"),"itemCode")
	            .add(Projections.groupProperty("itemBarcode"),"itemBarcode")
	            .add(Projections.groupProperty("itemName"),"itemName")
	            .add(Projections.sum("quantity"),"quantity")
	            .add(Projections.groupProperty("stockOnhand"),"stockOnhand")
	            .add(Projections.groupProperty("minimumStock"),"minimumStock")
		
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportItem.class));
		List<ReportItem> result = (List<ReportItem>) getHibernateTemplate().findByCriteria(criteria);
		
		LOGGER.debug("result="+result);
		return result.size();
	}

	@Override
	public List findByCriteriaReportSale(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 10;
		
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSale.class, "report");
		
		if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
			criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
		}
		if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
			criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
		}
		
		if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
			criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
		}
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("saler"),"saler")
	            .add(Projections.sum("subTotalAmount"),"subTotalAmount")
	            .add(Projections.sum("rebateAmount"),"rebateAmount")
	            .add(Projections.sum("vatAmount"),"vatAmount")
	            .add(Projections.sum("totalAmount"),"totalAmount")
	            .add(Projections.sum("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.sum("itemCostAmount"),"itemCostAmount")
	            .add(Projections.sum("contritutionAmount"),"contritutionAmount")
	            .add(Projections.sum("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSale.class));
		criteria.addOrder(Order.desc("totalAmount"));
		List<ReportSale> result = (List<ReportSale>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public Integer findItemSizeReportSale(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				DetachedCriteria criteria = DetachedCriteria.forClass(ReportSale.class, "report");
				
				if(!String.valueOf(criteriaMap.get("startDate")).equals("null")){
					criteria.add(Restrictions.ge("saleDate", String.valueOf(criteriaMap.get("startDate")) ));
				}
				if(!String.valueOf(criteriaMap.get("endDate")).equals("null")){
					criteria.add(Restrictions.le("saleDate", String.valueOf(criteriaMap.get("endDate")) ));
				}
				
				if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
					criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
				}
				
				criteria.setProjection(Projections.projectionList()
			            .add(Projections.groupProperty("saler"),"saler")
			            .add(Projections.sum("subTotalAmount"),"subTotalAmount")
			            .add(Projections.sum("rebateAmount"),"rebateAmount")
			            .add(Projections.sum("vatAmount"),"vatAmount")
			            .add(Projections.sum("totalAmount"),"totalAmount")
			            .add(Projections.sum("subTotalAfterRebate"),"subTotalAfterRebate")
			            .add(Projections.sum("itemCostAmount"),"itemCostAmount")
			            .add(Projections.sum("contritutionAmount"),"contritutionAmount")
			            .add(Projections.sum("marginAmount"),"marginAmount")
			            );  
				criteria.setResultTransformer( Transformers.aliasToBean(ReportSale.class));
				List<ReportSale> result = (List<ReportSale>) getHibernateTemplate().findByCriteria(criteria);
				
				LOGGER.debug("result="+result);
				return result.size();
	}

	@Override
	public Integer findItemSizeReportSummary(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSummary.class, "report");
		
		
		if(!String.valueOf(criteriaMap.get("year")).equals("null")){
			criteria.add(Restrictions.ilike("dataYear", "%"+String.valueOf(criteriaMap.get("year")+"%")));
		}
		
		criteria.add(Restrictions.isNotNull("dataYear"));
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("monthName"),"monthName")
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("contritutionAmount"),"contritutionAmount")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSummary.class));
		criteria.addOrder(Order.desc("saleMonthNumberOfDisplay"));
		List<ReportSummary> result = (List<ReportSummary>) getHibernateTemplate().findByCriteria(criteria);
		
		LOGGER.debug("result="+result);
		return result.size();
	}

	@Override
	public List findByCriteriaReportSummary(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		int firstResult = 0;
		int maxResults = 17;
		
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResults")).equals("null")){
			maxResults = Integer.parseInt(String.valueOf(criteriaMap.get("maxResults")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ReportSummary.class, "report");
		
		
		if(!String.valueOf(criteriaMap.get("year")).equals("null")){
			criteria.add(Restrictions.ilike("dataYear", "%"+String.valueOf(criteriaMap.get("year")+"%")));
		}
		

		criteria.add(Restrictions.isNotNull("dataYear"));
		
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.property("monthName"),"monthName")
	            .add(Projections.property("subTotalAmount"),"subTotalAmount")
	            .add(Projections.property("rebateAmount"),"rebateAmount")
	            .add(Projections.property("vatAmount"),"vatAmount")
	            .add(Projections.property("totalAmount"),"totalAmount")
	            .add(Projections.property("subTotalAfterRebate"),"subTotalAfterRebate")
	            .add(Projections.property("itemCostAmount"),"itemCostAmount")
	            .add(Projections.property("contritutionAmount"),"contritutionAmount")
	            .add(Projections.property("marginAmount"),"marginAmount")
	            );  
		criteria.setResultTransformer( Transformers.aliasToBean(ReportSummary.class));
		criteria.addOrder(Order.desc("saleMonthNumberOfDisplay"));
		List<ReportSummary> result = (List<ReportSummary>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResults);
		
		LOGGER.debug("result="+result);
		return result;
	}

}
