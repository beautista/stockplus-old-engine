package com.spt.engine.dao;


import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.spt.engine.controller.ItemController;
import com.spt.engine.entity.Item;
import com.spt.engine.entity.RunningNumber;
import com.spt.engine.entity.SalesTransaction;

@Repository
@Transactional
public class SalesTransactionDaoImpl  extends HibernateDaoSupport implements SalesTransactionDao{ 

	static final Logger LOGGER = LoggerFactory.getLogger(SalesTransactionDaoImpl.class);
	
	@Autowired
	public SalesTransactionDaoImpl(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	public Session getSession() {
	        return getSessionFactory().getCurrentSession();
	}

	@Override
	public SalesTransaction getById(Long id) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().get(SalesTransaction.class, id);
	}

	@Override
	public List<SalesTransaction> findByCriteria(Map<String, Object> criteriaMap) {
		int firstResult = 0;
		int maxResult = 10;
		
		if(!String.valueOf(criteriaMap.get("firstResult")).equals("null")){
			firstResult = Integer.parseInt(String.valueOf(criteriaMap.get("firstResult")));
		}
		
		if(!String.valueOf(criteriaMap.get("maxResult")).equals("null")){
			maxResult = Integer.parseInt(String.valueOf(criteriaMap.get("maxResult")));
		}
		
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(SalesTransaction.class, "tx");
		
		if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
			criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
		}

		if(!String.valueOf(criteriaMap.get("itemCode")).equals("null")){
			criteria.add(Restrictions.ilike("itemCode", "%"+String.valueOf(criteriaMap.get("itemCode")+"%")));
		}
		
		criteria.addOrder(Order.desc("id"));
		List<SalesTransaction> result = (List<SalesTransaction>) getHibernateTemplate().findByCriteria(criteria,firstResult,maxResult);
		
		LOGGER.debug("result="+result);
		return result;
	}

	@Override
	public void save(SalesTransaction salesTransaction) {
		// TODO Auto-generated method stub
		getHibernateTemplate().saveOrUpdate(salesTransaction);
	}

	@Override
	public void deleteSalesTransactionById(Long id) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(getHibernateTemplate().get(SalesTransaction.class, id));
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
				Criteria criteria = getSession().createCriteria(SalesTransaction.class, "item");
				
				if(!String.valueOf(criteriaMap.get("saler")).equals("null")){
					criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(criteriaMap.get("saler")+"%")));
				}
				
				criteria.setProjection(Projections.count("id"));
				Integer result = Integer.valueOf(String.valueOf(((Criteria)criteria).uniqueResult()));
				return result;
	}

	@Override
	public void deleteSalesTransactionByUser(String saler) {
		// TODO Auto-generated method stub
		Integer result = getHibernateTemplate().execute(new HibernateCallback<Integer>() {

			@Override
			public Integer doInHibernate(Session session) throws HibernateException {
				// TODO Auto-generated method stub
				Query queryDelete = session.createQuery("DELETE SalesTransaction WHERE saler='"+saler+"'");
				return queryDelete.executeUpdate();
			}
			
		});
	}

	@Override
	public List<SalesTransaction> findBySaler(String saler) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				DetachedCriteria criteria = DetachedCriteria.forClass(SalesTransaction.class, "tx");
				
				if(!String.valueOf(saler).equals("null")){
					criteria.add(Restrictions.ilike("saler", "%"+String.valueOf(saler+"%")));
				}

				
				criteria.addOrder(Order.asc("id"));
				List<SalesTransaction> result = (List<SalesTransaction>) getHibernateTemplate().findByCriteria(criteria);
				
				LOGGER.debug("result="+result);
				return result;
	}

	


	
}
