package com.spt.engine.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserAuthenController {

	@GetMapping("/user/{token}")
	public List authenPathToken(@PathVariable("token") String requestToken) {
		List listMap = new ArrayList();
		try {
			String base64ClientCredentials = new String(Base64.getDecoder().decode(requestToken.getBytes()));

			if("admin:admin".equals(base64ClientCredentials)){//YWRtaW46YWRtaW4=
				Map userModel = new HashMap();
				userModel.put("AccessToken", "XXXX");
				listMap.add(userModel);
			}else if("ja:758211".equals(base64ClientCredentials)){//YWRtaW46YWRtaW4=
				Map userModel = new HashMap();
				userModel.put("AccessToken", "XXXX");
				listMap.add(userModel);
			}else if("joice:7395888".equals(base64ClientCredentials)){//YWRtaW46YWRtaW4=
				Map userModel = new HashMap();
				userModel.put("AccessToken", "XXXX");
				listMap.add(userModel);
			}else if("jery:4497072".equals(base64ClientCredentials)){//YWRtaW46YWRtaW4=
				Map userModel = new HashMap();
				userModel.put("AccessToken", "XXXX");
				listMap.add(userModel);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return listMap;
	}
	
	@GetMapping("/user/{username}/{password}")
	public List authenPathVariable(@PathVariable("username") String usename,
			                       @PathVariable("password") String password) {
		List listMap = new ArrayList();
		
		if("admin".equals(usename) && "admin".equals(password)){
			Map userModel = new HashMap();
			userModel.put("AccessToken", "XXXX");
			listMap.add(userModel);
		}
		
		
		return listMap;
	}
	
	@GetMapping({"/user","/user/"})
	public Map authenHeader(HttpServletRequest request) {
		Map userModel = new HashMap();
		String username = request.getHeader("userName");
		if("admin".equals(username)){
			List<Map<String,Object>> profile = new ArrayList<Map<String,Object>>();
			profile.add(new HashMap(){{put("role", "ROLE_ADMIN");}});
			userModel.put("username", "admin");
			userModel.put("accessToken", "21232f297a57a5a743894a0e4a801fc3");
			userModel.put("authorities", profile);
			userModel.put("accountNonExpired", false);
			userModel.put("credentialsNonExpired", false);
			userModel.put("accountNonLocked", true);
			userModel.put("expireTime", 1476786517328l);
			
		}else if("suriya_e".equals(username)){
			List<Map<String,Object>> profile = new ArrayList<Map<String,Object>>();
			profile.add(new HashMap(){{put("role", "ROLE_USER");}});
			userModel.put("username", "suriya_e");
			userModel.put("accessToken", "21232f297a57a5a743894a0e4a801fc3");
			userModel.put("authorities", profile);
			userModel.put("accountNonExpired", false);
			userModel.put("credentialsNonExpired", false);
			userModel.put("accountNonLocked", true);
			userModel.put("expireTime", 1476786517328l);
			
		}else if("cee".equals(username)){
			List<Map<String,Object>> profile = new ArrayList<Map<String,Object>>();
			profile.add(new HashMap(){{put("role", "ROLE_USER_LEVEL1");}});
			userModel.put("username", "cee");
			userModel.put("accessToken", "b4df9f494056d51f86c7f1a89850c467");
			userModel.put("authorities", profile);
			userModel.put("accountNonExpired", false);
			userModel.put("credentialsNonExpired", false);
			userModel.put("accountNonLocked", true);
			userModel.put("expireTime", 1476786517328l);
			
		}else if("noey".equals(username)){
			List<Map<String,Object>> profile = new ArrayList<Map<String,Object>>();
			profile.add(new HashMap(){{put("role", "ROLE_USER_LEVEL1");}});
			userModel.put("username", "noey");
			userModel.put("accessToken", "fb2fcd534b0ff3bbed73cc51df620323");
			userModel.put("authorities", profile);
			userModel.put("accountNonExpired", false);
			userModel.put("credentialsNonExpired", false);
			userModel.put("accountNonLocked", true);
			userModel.put("expireTime", 1476786517328l);
			
		}else if("joice".equals(username)){
			List<Map<String,Object>> profile = new ArrayList<Map<String,Object>>();
			profile.add(new HashMap(){{put("role", "ROLE_ADMIN");}});
			userModel.put("username", "joice");
			userModel.put("accessToken", "7731e5ab7ee77abb0d75e9035290a523");//456459
			userModel.put("authorities", profile);
			userModel.put("accountNonExpired", false);
			userModel.put("credentialsNonExpired", false);
			userModel.put("accountNonLocked", true);
			userModel.put("expireTime", 1476786517328l);
			
		}
		return userModel;
	}
	
}
