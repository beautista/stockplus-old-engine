package com.spt.engine.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spt.engine.dao.ItemDao;
import com.spt.engine.entity.Customer;
import com.spt.engine.entity.Item;
import com.spt.engine.service.CustomerService;
import com.spt.engine.service.ItemService;


@RestController
public class CustomerController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/customer/id/{id}")
	public Customer getCustomer(@PathVariable Long id) {
		return customerService.getCustomerById(id);
	}
	
	@GetMapping("/customer/code/{code}")
	public Customer getCustomerByCode(@PathVariable String code) {
		return customerService.getCustomerByCode(code);
	}
	
	@PostMapping(value = "/customer/save")
	public ResponseEntity createCustomer(@ModelAttribute Customer customer) {
		customerService.save(customer);
		return new ResponseEntity(customer, HttpStatus.OK);
	}

}
