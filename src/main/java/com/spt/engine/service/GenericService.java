package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import com.spt.engine.entity.Item;
import com.spt.engine.entity.SalesTransaction;
import com.spt.engine.entity.StockHistory;

public interface GenericService {

	public void save(Object entity);
	public void deleteById(String entityName,Long id);
	public void delete(Object entity);
	
}
