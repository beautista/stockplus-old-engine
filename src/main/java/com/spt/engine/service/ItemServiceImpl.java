package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spt.engine.dao.ItemDao;
import com.spt.engine.entity.Item;

@Service
public class ItemServiceImpl implements ItemService{


	@Autowired
	ItemDao itemDao;
	
	@Override
	public Item getItemById(Long id) {
		// TODO Auto-generated method stub
		return itemDao.getItemById(id);
	}

	@Override
	public List<Item> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return itemDao.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return itemDao.findItemSize(criteriaMap);
	}

	@Override
	public Item getItemByBarcode(String barcode) {
		// TODO Auto-generated method stub
		return itemDao.getItemByBarcode(barcode);
	}

	@Override
	public void save(Item item) {
		// TODO Auto-generated method stub
		itemDao.save(item);
	}

	@Override
	public void deleteItemById(Long id) {
		// TODO Auto-generated method stub
		itemDao.deleteItemById(id);
	}

}
