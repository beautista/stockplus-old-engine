package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spt.engine.dao.ReportDao;

@Service
public class ReportServiceImpl implements ReportService {
	

	@Autowired
	ReportDao reportDao;

	@Override
	public List findByCriteriaReportItem(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportDao.findByCriteriaReportItem(criteriaMap);
	}

	@Override
	public Integer findItemSizeReportItem(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportDao.findItemSizeReportItem(criteriaMap);
	}

	@Override
	public List findByCriteriaReportSale(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportDao.findByCriteriaReportSale(criteriaMap);
	}

	@Override
	public Integer findItemSizeReportSale(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportDao.findItemSizeReportSale(criteriaMap);
	}

	@Override
	public Integer findItemSizeReportSummary(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportDao.findItemSizeReportSummary(criteriaMap);
	}

	@Override
	public List findByCriteriaReportSummary(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return reportDao.findByCriteriaReportSummary(criteriaMap);
	}

}
