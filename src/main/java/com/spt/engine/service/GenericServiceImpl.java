package com.spt.engine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spt.engine.dao.GenericDao;

@Service
public class GenericServiceImpl implements GenericService {

	@Autowired
	GenericDao genericDao;
	
	@Override
	public void save(Object entity) {
		// TODO Auto-generated method stub
		genericDao.save(entity);

	}

	@Override
	public void deleteById(String entityName, Long id) {
		// TODO Auto-generated method stub

		genericDao.deleteById(entityName, id);
	}

	@Override
	public void delete(Object entity) {
		// TODO Auto-generated method stub

		genericDao.delete(entity);
	}

}
