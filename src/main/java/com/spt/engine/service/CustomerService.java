package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import com.spt.engine.entity.Customer;

public interface CustomerService {

	public Customer getCustomerById(Long id);
	public List<Customer> findByCriteria(Map<String,Object> criteriaMap);
	public Integer findCustomerSize(Map<String,Object> criteriaMap);
	public Customer getCustomerByCode(String code);
	public void save(Customer customer);
	public void deleteCustomerById(Long id);
	
}
