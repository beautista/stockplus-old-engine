package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spt.engine.dao.ItemDao;
import com.spt.engine.dao.SalesTransactionDao;
import com.spt.engine.entity.Item;
import com.spt.engine.entity.SalesTransaction;

@Service
public class SalesTransactionServiceImpl implements SalesTransactionService{


	@Autowired
	SalesTransactionDao salesTransactionDao;

	@Override
	public SalesTransaction getById(Long id) {
		// TODO Auto-generated method stub
		return salesTransactionDao.getById(id);
	}

	@Override
	public List<SalesTransaction> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesTransactionDao.findByCriteria(criteriaMap);
	}

	@Override
	public void save(SalesTransaction salesTransaction) {
		// TODO Auto-generated method stub
		salesTransactionDao.save(salesTransaction);
	}

	@Override
	public void deleteSalesTransactionById(Long id) {
		// TODO Auto-generated method stub
		salesTransactionDao.deleteSalesTransactionById(id);
	}

	@Override
	public Integer findSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesTransactionDao.findSize(criteriaMap);
	}

	@Override
	public void deleteSalesTransactionByUser(String saler) {
		// TODO Auto-generated method stub
		salesTransactionDao.deleteSalesTransactionByUser(saler);
	}

	@Override
	public List<SalesTransaction> findBySaler(String saler) {
		// TODO Auto-generated method stub
		return salesTransactionDao.findBySaler(saler);
	}
	
	

}
