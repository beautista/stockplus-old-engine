package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spt.engine.dao.CustomerDao;
import com.spt.engine.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDao customerDao;
	
	@Override
	public Customer getCustomerById(Long id) {
		// TODO Auto-generated method stub
		return customerDao.getCustomerById(id);
	}

	@Override
	public List<Customer> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return customerDao.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return customerDao.findCustomerSize(criteriaMap);
	}

	@Override
	public Customer getCustomerByCode(String code) {
		// TODO Auto-generated method stub
		return customerDao.getCustomerByCode(code);
	}

	@Override
	public void save(Customer customer) {
		// TODO Auto-generated method stub

		customerDao.save(customer);
	}

	@Override
	public void deleteCustomerById(Long id) {
		// TODO Auto-generated method stub

		customerDao.deleteCustomerById(id);
	}

}
