package com.spt.engine.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spt.engine.dao.SalesOrderDao;
import com.spt.engine.entity.SaleOrder;
import com.spt.engine.entity.SaleOrderItems;

@Service
public class SalesOrderServiceImpl implements SalesOrderService {

	@Autowired
	SalesOrderDao salesOrderDao;
	
	@Override
	public SaleOrder getSaleOrderById(Long id) {
		// TODO Auto-generated method stub
		return salesOrderDao.getSaleOrderById(id);
	}

	@Override
	public List<SaleOrder> findByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesOrderDao.findByCriteria(criteriaMap);
	}

	@Override
	public Integer findSaleOrderSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderSize(criteriaMap);
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderItemByCriteria(criteriaMap);
	}

	@Override
	public Integer findSaleOrderItemSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderItemSize(criteriaMap);
	}
	

	@Override
	public List<SaleOrderItems> findSaleOrderItemCustomerByCriteria(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderItemCustomerByCriteria(criteriaMap);
	}

	@Override
	public Integer findSaleOrderItemCustomerSize(Map<String, Object> criteriaMap) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderItemCustomerSize(criteriaMap);
	}

	@Override
	public SaleOrder getSaleOrderByNumber(String number) {
		// TODO Auto-generated method stub
		return salesOrderDao.getSaleOrderByNumber(number);
	}

	@Override
	public void save(SaleOrder saleOrder) {
		// TODO Auto-generated method stub

		salesOrderDao.save(saleOrder);
	}

	@Override
	public void save(SaleOrderItems saleOrderItems) {
		// TODO Auto-generated method stub

		salesOrderDao.save(saleOrderItems);
	}

	@Override
	public void deleteSaleOrderById(Long id) {
		// TODO Auto-generated method stub

		salesOrderDao.deleteSaleOrderById(id);
	}

	@Override
	public void deleteSaleOrderItemsById(Long id) {
		// TODO Auto-generated method stub

		salesOrderDao.deleteSaleOrderItemsById(id);
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoNumber(String soNumber) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderItemBySoNumber(soNumber);
	}

	@Override
	public List<SaleOrderItems> findSaleOrderItemBySoId(Long soId) {
		// TODO Auto-generated method stub
		return salesOrderDao.findSaleOrderItemBySoId(soId);
	}

	@Override
	public List<SaleOrderItems> getSaleOrderItemByNumber(String number) {
		// TODO Auto-generated method stub
		return salesOrderDao.getSaleOrderItemByNumber(number);
	}

}
