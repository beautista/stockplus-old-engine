package com.spt.engine.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="tns_running_number")
public class RunningNumber implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5447223319761455070L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="rnn_id")
	private Long id;
	
	@Column(name="rnn_code")
	private String code;
	
	@Column(name="rnn_running")
	private Integer running;
	
	@Column(name="rnn_created_user")
	private String createdBy;
	
	@Column(name="rnn_created_time")
	private Timestamp createdDate;
	
	@Column(name="rnn_update_user")
	private String updateBy;
	
	@Column(name="rnn_update_time")
	private Timestamp updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getRunning() {
		return running;
	}

	public void setRunning(Integer running) {
		this.running = running;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	
	
	
	
	
}
