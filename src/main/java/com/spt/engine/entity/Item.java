package com.spt.engine.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name="tns_item")
public class Item implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5615796191512178504L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="itm_id")
	private Long id;
	
	@Column(name="itm_code")
	private String itemCode;
	
	@Column(name="itm_barcode")
	private String itemBarcode;
	
	@Column(name="itm_name")
	private String name;
	
	@Column(name="itm_stock")
	@ColumnDefault("'0'")
	private Integer stockOnHand;
	
	@Column(name="itm_min_stock")
	@ColumnDefault("'0'")
	private Integer minimumStock;

	@Column(name="itm_sell_price")
	@ColumnDefault("'0.0'")
	private Float sellingPriceAmount;
	
	@Column(name="itm_nor_sell_price")
	@ColumnDefault("'0.0'")
	private Float normalSellingPriceAmount;
	
	@Column(name="itm_pro_sell_price")
	@ColumnDefault("'0.0'")
	private Float promotionSellingPriceAmount;
	
	@Column(name="itm_pro_sell_price_pct")
	@ColumnDefault("'0.0'")
	private Float promotionSellingPricePercentage;
	
	@Column(name="itm_pur_price")
	@ColumnDefault("'0.0'")
	private Float purchasePriceAmount;
	
	@Column(name="itm_nor_pur_price")
	@ColumnDefault("'0.0'")
	private Float normalPurchasePriceAmount;
	
	@Column(name="itm_pro_pur_price")
	@ColumnDefault("'0.0'")
	private Float promotionPurchasePriceAmount;
	
	@Column(name="itm_pro_pur_price_pt")
	@ColumnDefault("'0.0'")
	private Float promotionPurchasePricePercentage;
	
	@Column(name="itm_created_user")
	private String createdBy;
	
	@Column(name="itm_created_time")
	private Timestamp createdDate;
	
	@Column(name="itm_update_user")
	private String updateBy;
	
	@Column(name="itm_update_time")
	private Timestamp updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemBarcode() {
		return itemBarcode;
	}

	public void setItemBarcode(String itemBarcode) {
		this.itemBarcode = itemBarcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStockOnHand() {
		return stockOnHand;
	}

	public void setStockOnHand(Integer stockOnHand) {
		this.stockOnHand = stockOnHand;
	}

	public Integer getMinimumStock() {
		return minimumStock;
	}

	public void setMinimumStock(Integer minimumStock) {
		this.minimumStock = minimumStock;
	}

	public Float getSellingPriceAmount() {
		return sellingPriceAmount;
	}

	public void setSellingPriceAmount(Float sellingPriceAmount) {
		this.sellingPriceAmount = sellingPriceAmount;
	}

	public Float getNormalSellingPriceAmount() {
		return normalSellingPriceAmount;
	}

	public void setNormalSellingPriceAmount(Float normalSellingPriceAmount) {
		this.normalSellingPriceAmount = normalSellingPriceAmount;
	}

	public Float getPromotionSellingPriceAmount() {
		return promotionSellingPriceAmount;
	}

	public void setPromotionSellingPriceAmount(Float promotionSellingPriceAmount) {
		this.promotionSellingPriceAmount = promotionSellingPriceAmount;
	}

	public Float getPromotionSellingPricePercentage() {
		return promotionSellingPricePercentage;
	}

	public void setPromotionSellingPricePercentage(Float promotionSellingPricePercentage) {
		this.promotionSellingPricePercentage = promotionSellingPricePercentage;
	}

	public Float getPurchasePriceAmount() {
		return purchasePriceAmount;
	}

	public void setPurchasePriceAmount(Float purchasePriceAmount) {
		this.purchasePriceAmount = purchasePriceAmount;
	}

	public Float getNormalPurchasePriceAmount() {
		return normalPurchasePriceAmount;
	}

	public void setNormalPurchasePriceAmount(Float normalPurchasePriceAmount) {
		this.normalPurchasePriceAmount = normalPurchasePriceAmount;
	}

	public Float getPromotionPurchasePriceAmount() {
		return promotionPurchasePriceAmount;
	}

	public void setPromotionPurchasePriceAmount(Float promotionPurchasePriceAmount) {
		this.promotionPurchasePriceAmount = promotionPurchasePriceAmount;
	}

	public Float getPromotionPurchasePricePercentage() {
		return promotionPurchasePricePercentage;
	}

	public void setPromotionPurchasePricePercentage(Float promotionPurchasePricePercentage) {
		this.promotionPurchasePricePercentage = promotionPurchasePricePercentage;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	
	
}
