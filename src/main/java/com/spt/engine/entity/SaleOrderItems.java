package com.spt.engine.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="tns_sale_order_items")
public class SaleOrderItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1885360942170571292L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sod_id")
	private Long id;

	@Column(name="sod_so_id")
	private Long soId;
	
	@Column(name="sod_so_number")
	private String soNumber;
	
	@Column(name="sod_item_code")
	private String itemCode;
	
	@Column(name="sod_item_barcode")
	private String itemBarode;

	@Column(name="sod_item_name")
	private String itemName;
	
	@Column(name="so_selling_price_amt")
	@ColumnDefault("'0.0'")
	private Float sellingPriceAmount;
	
	@Column(name="so_quantity")
	@ColumnDefault("'0'")
	private Integer quantity;
	
	@Column(name="so_total_amt")
	@ColumnDefault("'0.0'")
	private Float totalAmount;
	
	@Column(name="sod_price_type")
	private String priceType;
	
	@Column(name="sod_created_user")
	private String createdBy;
	
	@JsonFormat(pattern="dd-MM-yyyy hh:mm",timezone="GMT+7")
	@Column(name="sod_created_time")
	private Timestamp createdDate;
	
	@Column(name="sod_update_user")
	private String updateBy;
	
	@Column(name="sod_update_time")
	private Timestamp updateDate;
	

	@Column(name="sod_customer_code")
	private String customerCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSoId() {
		return soId;
	}

	public void setSoId(Long soId) {
		this.soId = soId;
	}

	public String getSoNumber() {
		return soNumber;
	}

	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemBarode() {
		return itemBarode;
	}

	public void setItemBarode(String itemBarode) {
		this.itemBarode = itemBarode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Float getSellingPriceAmount() {
		return sellingPriceAmount;
	}

	public void setSellingPriceAmount(Float sellingPriceAmount) {
		this.sellingPriceAmount = sellingPriceAmount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public Float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	
	
}
