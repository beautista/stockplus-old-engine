package com.spt.engine.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name="mst_customer")
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3633217926871908636L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ctm_id")
	private Long id;
	
	@Column(name="ctm_code")
	private String customerCode;
	
	@Column(name="ctm_barcode")
	private String customerBarcode;
	
	@Column(name="ctm_firstname")
	private String firstname;
	
	@Column(name="ctm_lastname")
	private String lastname;
	
	@Column(name="ctm_point")
	@ColumnDefault("'0'")
	private Integer point;
	
	@Column(name="ctm_created_user")
	private String createdBy;
	
	@Column(name="ctm_created_time")
	private Timestamp createdDate;
	
	@Column(name="ctm_update_user")
	private String updateBy;
	
	@Column(name="ctm_update_time")
	private Timestamp updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerBarcode() {
		return customerBarcode;
	}

	public void setCustomerBarcode(String customerBarcode) {
		this.customerBarcode = customerBarcode;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}
	
	
}
