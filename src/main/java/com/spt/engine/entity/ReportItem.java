package com.spt.engine.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

@Entity
@Immutable
public class ReportItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3385326482052869325L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "itemcode", updatable = false, nullable = false)
	private String itemCode;
	
	@Column(name = "barcode", updatable = false)
	private String itemBarcode;
	
	@Column(name = "itemname", updatable = false)
	private String itemName;
	
	@Column(name = "quantity", updatable = false)
	private String quantity;
	
	@Column(name = "stock", updatable = false)
	private String stockOnhand;
	
	@Column(name = "minstock", updatable = false)
	private String minimumStock;
	
	@Column(name = "saledate", updatable = false)
	private String saleDate;
	
	

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemBarcode() {
		return itemBarcode;
	}

	public void setItemBarcode(String itemBarcode) {
		this.itemBarcode = itemBarcode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getStockOnhand() {
		return stockOnhand;
	}

	public void setStockOnhand(String stockOnhand) {
		this.stockOnhand = stockOnhand;
	}

	public String getMinimumStock() {
		return minimumStock;
	}

	public void setMinimumStock(String minimumStock) {
		this.minimumStock = minimumStock;
	}

	public String getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	
	
	
}
