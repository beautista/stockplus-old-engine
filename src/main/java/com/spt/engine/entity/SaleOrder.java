package com.spt.engine.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="tns_sale_order")
public class SaleOrder implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1378424612578416125L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="so_id")
	private Long id;
	
	@Column(name="so_number")
	private String soNumber;
	
	@Column(name="so_customer_code")
	private String customerCode;
	
	@Column(name="so_saler")
	private String saler;
	
	@JsonFormat(pattern="dd-MM-yyyy hh:mm",timezone="GMT+7")
	@Column(name="so_sale_time")
	private Timestamp saleDate;
	
	@Column(name="so_sale_date")
	private String saleDateString;
	
	@Column(name="so_sale_period")
	private String salePeriod;
	
	@Column(name="so_item_count")
	@ColumnDefault("'0'")
	private Integer itemCount;
	
	@Column(name="so_subtotal_amt")
	@ColumnDefault("'0.0'")
	private Float subTotalAmount;
	
	@Column(name="so_rebate_amt")
	@ColumnDefault("'0.0'")
	private Float rebateAmount;
	
	@Column(name="so_subtotal_amt_after_rebate")
	@ColumnDefault("'0.0'")
	private Float subTotalAfterRebateAmount;
	
	@Column(name="so_vat_amt")
	@ColumnDefault("'0.0'")
	private Float vatAmount;
	
	@Column(name="so_subtotal_amt_after_vat")
	@ColumnDefault("'0.0'")
	private Float subTotalAfterVatAmount;
	
	@Column(name="so_total_amt")
	@ColumnDefault("'0.0'")
	private Float totalAmount;
	
	@Column(name="so_tended_amt")
	@ColumnDefault("'0.0'")
	private Float tendedAmount;
	
	@Column(name="so_change_amt")
	@ColumnDefault("'0.0'")
	private Float changeAmount;

	@Column(name="so_round_type")
	private String roundType;
	
	@Column(name="so_payment_type")
	private String paymentType;
	
	@Column(name="so_item_cost_amt")
	@ColumnDefault("'0.0'")
	private Float itemCostAmount;
	
	@Column(name="so_fix_cost_pct")
	@ColumnDefault("'0.0'")
	private Float fixCostPercentage;
	
	@Column(name="so_fix_cost_amt")
	@ColumnDefault("'0.0'")
	private Float fixCostAmount;
	
	@Column(name="so_cont_pct")
	@ColumnDefault("'0.0'")
	private Float contributionPercentage;
	
	@Column(name="so_cont_amt")
	@ColumnDefault("'0.0'")
	private Float contributionAmount;
	
	@Column(name="so_margin_pct")
	@ColumnDefault("'0.0'")
	private Float marginPercentage;
	
	@Column(name="so_margin_amt")
	@ColumnDefault("'0.0'")
	private Float marginAmount;
	
	@Column(name="so_created_user")
	private String createdBy;
	
	@Column(name="so_created_time")
	private Timestamp createdDate;
	
	@Column(name="so_update_user")
	private String updateBy;
	
	@Column(name="so_update_time")
	private Timestamp updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSoNumber() {
		return soNumber;
	}

	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getSaler() {
		return saler;
	}

	public void setSaler(String saler) {
		this.saler = saler;
	}

	public Timestamp getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Timestamp saleDate) {
		this.saleDate = saleDate;
	}

	public Integer getItemCount() {
		return itemCount;
	}

	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}

	public Float getSubTotalAmount() {
		return subTotalAmount;
	}

	public void setSubTotalAmount(Float subTotalAmount) {
		this.subTotalAmount = subTotalAmount;
	}

	public Float getRebateAmount() {
		return rebateAmount;
	}

	public void setRebateAmount(Float rebateAmount) {
		this.rebateAmount = rebateAmount;
	}

	public Float getSubTotalAfterRebateAmount() {
		return subTotalAfterRebateAmount;
	}

	public void setSubTotalAfterRebateAmount(Float subTotalAfterRebateAmount) {
		this.subTotalAfterRebateAmount = subTotalAfterRebateAmount;
	}

	public Float getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(Float vatAmount) {
		this.vatAmount = vatAmount;
	}

	public Float getSubTotalAfterVatAmount() {
		return subTotalAfterVatAmount;
	}

	public void setSubTotalAfterVatAmount(Float subTotalAfterVatAmount) {
		this.subTotalAfterVatAmount = subTotalAfterVatAmount;
	}

	public Float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Float getTendedAmount() {
		return tendedAmount;
	}

	public void setTendedAmount(Float tendedAmount) {
		this.tendedAmount = tendedAmount;
	}

	public Float getChangeAmount() {
		return changeAmount;
	}

	public void setChangeAmount(Float changeAmount) {
		this.changeAmount = changeAmount;
	}

	public String getRoundType() {
		return roundType;
	}

	public void setRoundType(String roundType) {
		this.roundType = roundType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Float getItemCostAmount() {
		return itemCostAmount;
	}

	public void setItemCostAmount(Float itemCostAmount) {
		this.itemCostAmount = itemCostAmount;
	}

	public Float getFixCostPercentage() {
		return fixCostPercentage;
	}

	public void setFixCostPercentage(Float fixCostPercentage) {
		this.fixCostPercentage = fixCostPercentage;
	}

	public Float getFixCostAmount() {
		return fixCostAmount;
	}

	public void setFixCostAmount(Float fixCostAmount) {
		this.fixCostAmount = fixCostAmount;
	}

	public Float getContributionPercentage() {
		return contributionPercentage;
	}

	public void setContributionPercentage(Float contributionPercentage) {
		this.contributionPercentage = contributionPercentage;
	}

	public Float getContributionAmount() {
		return contributionAmount;
	}

	public void setContributionAmount(Float contributionAmount) {
		this.contributionAmount = contributionAmount;
	}

	public Float getMarginPercentage() {
		return marginPercentage;
	}

	public void setMarginPercentage(Float marginPercentage) {
		this.marginPercentage = marginPercentage;
	}

	public Float getMarginAmount() {
		return marginAmount;
	}

	public void setMarginAmount(Float marginAmount) {
		this.marginAmount = marginAmount;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public String getSaleDateString() {
		return saleDateString;
	}

	public void setSaleDateString(String saleDateString) {
		this.saleDateString = saleDateString;
	}

	public String getSalePeriod() {
		return salePeriod;
	}

	public void setSalePeriod(String salePeriod) {
		this.salePeriod = salePeriod;
	}

	
	
}
