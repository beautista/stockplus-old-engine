package com.spt.engine.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name="tns_sales_transaction")
public class SalesTransaction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4818169493308432634L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sts_id")
	private Long id;
	
	@Column(name="sts_saler")
	private String saler;
	
	@Column(name="sts_item_id")
	private Long itemId;
	

	@Column(name="sts_item_code")
	private String itemCode;

	@Column(name="sts_item_barcode")
	private String itemBarcode;

	@Column(name="sts_item_name")
	private String itemName;
	

	@Column(name="sts_quantity")
	@ColumnDefault("'0'")
	private Integer quantity;
	
	@Column(name="sts_sell_price")
	@ColumnDefault("'0.0'")
	private Float sellingPriceAmount;
	

	@Column(name="sts_price_type")//,columnDefinition="COMMENT 'N=Normal , P=Promotion' "
	private String priceType;
	

	@Column(name="sts_total_sell_price")
	@ColumnDefault("'0.0'")
	private Float totalSellingPriceAmount;
	
	@Column(name="sts_total_pur_price")
	@ColumnDefault("'0.0'")
	private Float totalPuechasePriceAmount;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getSaler() {
		return saler;
	}


	public void setSaler(String saler) {
		this.saler = saler;
	}


	public Long getItemId() {
		return itemId;
	}


	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public Float getSellingPriceAmount() {
		return sellingPriceAmount;
	}


	public void setSellingPriceAmount(Float sellingPriceAmount) {
		this.sellingPriceAmount = sellingPriceAmount;
	}


	public String getPriceType() {
		return priceType;
	}


	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}


	public Float getTotalSellingPriceAmount() {
		return totalSellingPriceAmount;
	}


	public void setTotalSellingPriceAmount(Float totalSellingPriceAmount) {
		this.totalSellingPriceAmount = totalSellingPriceAmount;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getItemBarcode() {
		return itemBarcode;
	}


	public void setItemBarcode(String itemBarcode) {
		this.itemBarcode = itemBarcode;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public Float getTotalPuechasePriceAmount() {
		return totalPuechasePriceAmount;
	}


	public void setTotalPuechasePriceAmount(Float totalPuechasePriceAmount) {
		this.totalPuechasePriceAmount = totalPuechasePriceAmount;
	}

	
}
