package com.spt.engine.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name="tns_history_stock")
public class StockHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1885360942170571292L;
	public static final String TRANSACTION_TYPE_IN= "I";
	public static final String TRANSACTION_TYPE_OUT= "O";
	

	public static final String ACTIVITY_TYPE_SALE_ORDER= "SO";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="hts_id")
	private Long id;

	@Column(name="hts_transaction_type")//,columnDefinition="COMMENT 'I=In , O=Out' "
	private String transactionType;
	
	@Column(name="hts_activity_type")
	private String activityType;
	
	@Column(name="hts_activity_number")
	private String activityNumber;

	@Column(name="hts_quantity")
	@ColumnDefault("'0'")
	private Integer quantity;
	
	@Column(name="hts_created_user")
	private String createdBy;
	
	@Column(name="hts_created_time")
	private Timestamp createdDate;
	
	@Column(name="hts_update_user")
	private String updateBy;
	
	@Column(name="hts_update_time")
	private Timestamp updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityNumber() {
		return activityNumber;
	}

	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	
	
}
