package com.spt.engine.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

@Entity
@Immutable
public class ReportSummary implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3385326482052869325L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "salemonthnumdisp", updatable = false, nullable = false)
	private String saleMonthNumberOfDisplay;
	
	@Column(name = "monthname", updatable = false)
	private String monthName;
	
	@Column(name = "datayear", updatable = false)
	private String dataYear;
	
	@Column(name = "subtotalamount", updatable = false)
	private String subTotalAmount;
	
	@Column(name = "rebateamount", updatable = false)
	private String rebateAmount;
	
	@Column(name = "vatamount", updatable = false)
	private String vatAmount;
	
	@Column(name = "totalamount", updatable = false)
	private String totalAmount;
	
	@Column(name = "subtotalafterrebate", updatable = false)
	private String subTotalAfterRebate;
	
	@Column(name = "itemcost", updatable = false)
	private String itemCostAmount;
	
	@Column(name = "contamount", updatable = false)
	private String contritutionAmount;
	
	@Column(name = "marginamount", updatable = false)
	private String marginAmount;

	public String getSaleMonthNumberOfDisplay() {
		return saleMonthNumberOfDisplay;
	}

	public void setSaleMonthNumberOfDisplay(String saleMonthNumberOfDisplay) {
		this.saleMonthNumberOfDisplay = saleMonthNumberOfDisplay;
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public String getDataYear() {
		return dataYear;
	}

	public void setDataYear(String dataYear) {
		this.dataYear = dataYear;
	}

	public String getSubTotalAmount() {
		return subTotalAmount;
	}

	public void setSubTotalAmount(String subTotalAmount) {
		this.subTotalAmount = subTotalAmount;
	}

	public String getRebateAmount() {
		return rebateAmount;
	}

	public void setRebateAmount(String rebateAmount) {
		this.rebateAmount = rebateAmount;
	}

	public String getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(String vatAmount) {
		this.vatAmount = vatAmount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSubTotalAfterRebate() {
		return subTotalAfterRebate;
	}

	public void setSubTotalAfterRebate(String subTotalAfterRebate) {
		this.subTotalAfterRebate = subTotalAfterRebate;
	}

	public String getItemCostAmount() {
		return itemCostAmount;
	}

	public void setItemCostAmount(String itemCostAmount) {
		this.itemCostAmount = itemCostAmount;
	}

	public String getContritutionAmount() {
		return contritutionAmount;
	}

	public void setContritutionAmount(String contritutionAmount) {
		this.contritutionAmount = contritutionAmount;
	}

	public String getMarginAmount() {
		return marginAmount;
	}

	public void setMarginAmount(String marginAmount) {
		this.marginAmount = marginAmount;
	}

	
	
}
